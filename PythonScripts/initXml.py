import genXmlUtils as utils
import numpy as np
path = "../init.xml"
eXML = utils.ExperimentXML(path)

'''eXML.AddTrial("team",1,2,"random", "modal", numHubs = 5, numSites = 6)
eXML.AddTrial("team",1,2,"circle", "random", numHubs = 5)
eXML.AddTrial("hierarchy",1,2,"line", "random", numHubs = 5)
eXML.AddTrial("hierarchy",1,2,"staggered", "random", numHubs = 5)
'''

orgTypes = ["team","hierarchy"]
latencies =    np.arange(.05, 1, .10)
bandwidths =   np.arange(.05, 1, .10)
hubDistributions = ["random"] #, "line", "circle"]#staggered
siteDistributions = ["random"]#["random", "modal"]
siteNoiseList = [0.0]
consensusTeamBool = [True]
hubNumList = [4]
siteNumList = [8]
numDroppedList = [0]

nTrials = 5
for i in range(0, nTrials):
	for org in orgTypes:
		print(org)
		for lat in latencies:
			print(lat)
			for band in bandwidths:
				print(band)
				for hubDist in hubDistributions:
					for siteDist in siteDistributions:
						for siteNoise in siteNoiseList:
							for consensusType in consensusTeamBool:
								for nHubs in hubNumList:
									for nSites in siteNumList:
										for nd in numDroppedList:
											eXML.AddTrial(org, lat, band, hubDist, siteDist, siteNoise, consensusType, nHubs, nSites, nd)

for i in range(nTrials):
	for nHubs in hubNumList:
		for nSites in siteNumList:
			eXML.AddTrial("none", 0, 0, "random", "random", 0.0, True, nHubs, nSites)

eXML.Write()

import xml.dom.minidom
xml = xml.dom.minidom.parse(path)
#print(xml.toprettyxml())
