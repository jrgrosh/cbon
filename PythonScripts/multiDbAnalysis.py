import matplotlib.pyplot as plt
import pandas as pd
import sqlite3

path = "/mnt/c/Users/hcmi/Downloads/"
fileNames = ["4h8s0rte.db","4h8s25rte.db","4h8s50rte.db","4h8s75rte.db","4h8s100rte.db"]
NUM_HUBS = 4
indices = [0, .25, .5, .75, 1]
tAvg = []
hAvg = []
for fn in fileNames:
	con = sqlite3.connect(path + fn)
	cur = con.cursor()
	q1 = "SELECT distinct(site_id) as 'Chosen Site Id', quality, hub.f_trial_id from Hub inner join Site on hub.chosen_site_id = Site.site_id order by hub.f_trial_id asc, quality desc"
	cur.execute(q1)
	r_chosen_sites = (cur.fetchall())
	#print(r_chosen_sites)

	df_chosen_sites = pd.DataFrame(r_chosen_sites)
	df_chosen_sites.columns = ["Chosen Site ID", "Quality", "Trial ID"]
	#df_chosen_sites.set_index("Chosen Site ID", inplace = True)
	#print(df_chosen_sites)

	q2 = "SELECT distinct(site_id) as 'Known Site Id', quality, Site.f_trial_id from Site inner join SiteKnownByHub on site_id = known_site_id order by Site.f_trial_id asc, quality desc "
	cur.execute(q2)
	r_known_sites = (cur.fetchall())
	#print(r_known_sites)

	df_known_sites = pd.DataFrame(r_known_sites)
	df_known_sites.columns = ["Known Site ID", "Quality", "Trial ID"]
	#df_known_sites.set_index("Known Site ID", inplace = True)

	df_known_sites_trimmed = df_known_sites.groupby('Trial ID').head(NUM_HUBS)

	#^^^ Determine what the optimal convigurations is -given the knowledge of the group - , determine what was actually known

	#vvvv do metrics... percentage of optimal commits, proportion of optimal sites chosen TODO
	caks = df_chosen_sites.merge(df_known_sites_trimmed, on = 'Trial ID', how = 'outer')
	caks1 = (caks[caks['Chosen Site ID'] == caks['Known Site ID']].groupby('Trial ID').count())

	optimal_commit = caks1.apply(lambda x : (x == NUM_HUBS).astype(int))

	quality_of_approximation = caks1 / NUM_HUBS
	quality_of_approximation.columns = ['qoa','b','c','d']
	quality_of_approximation = quality_of_approximation.drop(columns = ['b','c','d'])

	optimal_commit.columns = ['propOfOptCommits', '_' ,'__', '___']
	optimal_commit = optimal_commit.drop(columns = ['_' ,'__', '___'])

	q3 = "SELECT trial_id, latency, bandwidth, orgType FROM TRIAL"
	cur.execute(q3)
	r_trials = (cur.fetchall())

	df_trials = pd.DataFrame(r_trials)
	print(df_trials.head())
	print(set(df_trials[3]))
	df_trials.columns = ["Trial ID", "Latency", "Bandwidth", "OrgType"]
	'''
	hack_dict = {0:0,3:5, 9:15, 16:25, 22:35, 29:45, 35:55,42:65,48:75,55:85, 61:95}
	df_trials['Latency'] = df_trials['Latency'].map(lambda x : hack_dict[x])
	df_trials['Bandwidth'] = df_trials['Bandwidth'].map(lambda x : hack_dict[x])
	'''
	df_trials.set_index("Trial ID", inplace = True)
	print(set(df_trials["OrgType"]))
	print(df_trials[df_trials.OrgType =='hierarchy'].head(50))
	print(optimal_commit.head(200))
	df_opt_trial = optimal_commit.join(df_trials, on = "Trial ID")
	print(df_opt_trial.head())
	print(set(df_opt_trial["OrgType"]))

	df_qoa = quality_of_approximation.join(df_trials)

	df_avs = df_opt_trial.groupby(["Latency", "Bandwidth", "OrgType"]).mean()
	df_std = df_opt_trial.groupby(["Latency", "Bandwidth", "OrgType"]).std()
	print(df_avs.head())
	
	df_avs = df_opt_trial.groupby(["OrgType"]).mean()
	df_std = df_opt_trial.groupby(["OrgType"]).std()
	print(df_avs.head())
	
	print(df_avs.index)
	print(df_avs["propOfOptCommits"])
	print(df_avs["propOfOptCommits"]["hierarchy"])
	
	hAvg.append(df_avs["propOfOptCommits"]["hierarchy"])
	tAvg.append(df_avs["propOfOptCommits"]["team"])
	
	#print(df_avs.loc('hierarchy'))
	#print(df_avs['hierarchy'])
print(hAvg)
print(tAvg)
plt.plot(indices,hAvg, label = "hierarchy")
plt.plot(indices,tAvg, label = "team")
plt.legend()
plt.xlabel("Return to exploring frequency")
plt.ylabel("Average Proportion of Optimal Commits")
plt.show()
