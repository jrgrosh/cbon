import matplotlib.pyplot as plt
import numpy as np
from scipy.special import expit
bandwidth_values = ['Low','Medium','High']
latency_values = ['Low','Medium','High']


fig = plt.figure()
ax = fig.add_subplot(1,1,1)

plt.title('Anticipated Effect of Bandwidth and Latency on Organizational Performance')
plt.xlabel('Latency')
plt.ylabel('Bandwidth')

val_range = ('0-20%', '20-40%', '40-60%', '60-80%', '80-100%')
plt.xticks(np.arange(len(val_range)), val_range)
plt.yticks(np.arange(len(val_range)), val_range)

x = []
y = []
for i in range(5):
	x.extend(range(5))
	for j in range(5):
		y.append(i)

performance = []
for i in range(25):
	performance.append((np.clip(np.round((1.75/(x[i]+y[i] + 1)), decimals = 2), .2, 1), (np.clip(np.round((1.5/(x[i]+y[i] + 1)), decimals = 2), .3, 1))))

plt.scatter(x,y, c='w')

for i in np.arange(len(performance)):
	x_offset = .1
	if((i+1)%3==0):
		x_offset += .1
	plt.annotate(performance[i],(x[i]-x_offset,y[i]))
plt.show()

