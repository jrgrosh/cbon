import xml.etree.cElementTree as ET
import numpy as np
'''
trial_id = 0

def main():
	root = ET.Element("root")
	doc = ET.SubElement(root, "doc")

	for i in range(10):
		t =  consensusTeamStatus,AddTrial(doc)
		AddHubsInCircle(t, 5)
		AddSites(t, 10)
	for i in range(10):
		t = AddTrial(doc)
		AddHubsInLine(t, 5)
		AddSites(t, 10)
	ET.SubElement(doc, "numTrials").text = str(trial_id)
	

	tree = ET.ElementTree(root)
	tree.write("../init.xml")
'''
class ExperimentXML:
	def __init__(self, path):
		self.filepath = path
		self.root = ET.Element("root")
		self.doc = ET.SubElement(self.root, "doc")
		self.numTrials = 0
		self.width = 76 * 1 / 3
		self.height = 76 * 1 / 3

	def Write(self):
		tree = ET.ElementTree(self.root)
		tree.write(self.filepath)

	def AddTrial(self, orgType, latency, bandwidth, hubDistribution, siteDistribution, siteNoiseStd = .3, consensusTeamStatus=True, numHubs = 3, numSites = 6, numDropped= 0):
		trial = ET.SubElement(self.doc, "trial")
		if(orgType != "hierarchy" and orgType != "team" and orgType != "none"):
			raise ValueError("Innappropriate argument value for orgType")
		ET.SubElement(trial, "orgType").text = orgType
		ET.SubElement(trial, "trial_id").text = str(self.numTrials)
		ET.SubElement(trial, "hubDistribution").text = hubDistribution
		ET.SubElement(trial, "siteDistribution").text = siteDistribution
		ET.SubElement(trial, "latency").text = str(latency)
		ET.SubElement(trial, "bandwidth").text = str(bandwidth)
		ET.SubElement(trial, "numHubs").text = str(numHubs)
		ET.SubElement(trial, "consensusTeamStatus").text = str(consensusTeamStatus)
		ET.SubElement(trial, "siteNoiseStd").text = str(siteNoiseStd)
		ET.SubElement(trial, "numDropped").text = str(numDropped)
		self.AddHubDistribution(trial, hubDistribution, numHubs)
		self.AddSiteDistribution(trial,siteDistribution, numSites)
	
		self.numTrials += 1
	
	
	def AddHubDistribution(self, trial, hubDistribution, numHubs):
		if(hubDistribution == "random"):
			self.AddRandUniformHubs(trial, numHubs)	
		elif(hubDistribution == "line"):
			self.AddHubsInLine(trial, numHubs)
		elif(hubDistribution == "circle"):
			self.AddHubsInCircle(trial, numHubs)
		elif(hubDistribution == "staggered"):
			self.AddHubsStaggered(trial, numHubs)
		else:
			raise ValueError("invalid hubDistribution argument")
			
	def AddHub(self, hubs, x, y, numSwarmAgents = 10):
		hub = ET.SubElement(hubs, "hub")
		x = np.around(x, decimals = 0)
		y = np.around(y, decimals = 0)
		ET.SubElement(hub, "x").text = str(x)
		ET.SubElement(hub, "y").text = str(y)
		ET.SubElement(hub, "numSwarmAgents").text = str(numSwarmAgents)
		
	
	def AddRandUniformHubs(self, trial, numHubs):
		hubs = ET.SubElement(trial, "hubs")
		for i in range(numHubs):
			self.AddHub(hubs, self.width*(.5 - np.random.random()), self.height*(.5 - np.random.random()))

	
	def AddHubsInCircle(self, trial, numHubs):
		hubs = ET.SubElement(trial, "hubs")
		for i in range(numHubs):
			theta = i * 2 * np.pi / float(numHubs)
			radius = 10.0
			self.AddHub(hubs, radius * np.cos(theta), radius * np.sin(theta))
	
	def AddHubsInLine(self, trial, numHubs):
		hubs = ET.SubElement(trial, "hubs")
		for i in range(numHubs):
			width = 5
			leftStart = -10
			self.AddHub(hubs, i*width + leftStart, 0)

	def AddHubsStaggered(self, trial, numHubs):
		hubs = ET.SubElement(trial, "hubs")
		for i in range(numHubs):
			width = 5
			height = 5
			leftStart = -10
			if(i%2 == 1):
				heightOffset = height
			else:
				heightOffset = 0
			self.AddHub(hubs, i*width + leftStart, heightOffset)
		
	
	def AddSiteDistribution(self, trial, siteDistribution, numSites):
		if(siteDistribution == "random"):
			self.AddRandUniformSites(trial, numSites)	
		elif(siteDistribution == "modal"):
			self.AddModalDistributedSites(trial, numSites)
		else:
			raise ValueError("invalid siteDistribution argument")
		

	def AddSite(self, sites, x, y, q):
		site = ET.SubElement(sites, "site")
		x = np.around(x, decimals = 0)
		y = np.around(y,decimals = 0)
		ET.SubElement(site, "x").text = str(x)
		ET.SubElement(site, "y").text =str(y)
		ET.SubElement(site, "q").text = str(q)



	def AddRandUniformSites(self, trial, numSites):
		sites = ET.SubElement(trial, "sites")
		for i in range(numSites):
			self.AddSite(sites, self.width*(.5 - np.random.random()), self.height*(.5-np.random.random()), np.random.random())

	def AddModalDistributedSites(self, trial, numSites):
		sites = ET.SubElement(trial, "sites")
		scale = 10
		modes = np.random.multivariate_normal([0,0], [[1,0],[0,1]], size = 2) * scale
		for i in range(numSites):
			sLocation = np.random.multivariate_normal(modes[i%2], [[1,0],[0,1]])
			self.AddSite(sites, sLocation[0], sLocation[1], np.random.random())
			

if __name__ == '__main__':
	main()
