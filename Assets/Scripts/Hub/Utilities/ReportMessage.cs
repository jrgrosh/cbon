﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class ReportMessage : Message {
	private HubState reportedState;
	public HierarchyHub recipient;

	public ReportMessage(HierarchyHub author, HubState hs, HierarchyHub r) : base(author) {
		reportedState = hs.Clone();
		recipient = r;
	}
	public HubState GetReportedState() {
		return reportedState;
	}

	public void SetHubState(HubState hs) {
		reportedState = hs.Clone();
	}

	public override string ToString() {
		//return base.ToString() + "{" + reportedState.ToString() + "}";
		string str = "ReportMessage(" + id.ToString() + " "; 
		if (receiveTime < 0) {
			str += "not yet received";
		} else {
			str += "has been received";//receiveTime;
		}
		str += " " + sendTime + " " + receiveTime + " "+sender.transform.position.ToString() + " " + recipient.transform.position.ToString() +  ", ";
		str += reportedState.ToString();
		str += ")";
		return str;
	}
}