﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrderMessage : Message {
	private GameObject orderSite;
	public HierarchyHub recipient;

	public OrderMessage(HierarchyHub author, GameObject s, HierarchyHub r) : base(author) {
		orderSite = s;
		recipient = r;
	}

	public GameObject GetOrderSite() {
		return orderSite;
	}

	public override string ToString() {
		//return base.ToString() + "{" + reportedState.ToString() + "}";
		string str = "OrderMessage(" + id.ToString() + " ";
		if (receiveTime < 0) {
			str += "not yet received";
		} else {
			str += "has been received";//receiveTime;
		}
		str += "send " + sendTime + " receive " + receiveTime + " sender " + sender.transform.position.ToString() + " receiver " + recipient.transform.position.ToString() + ", ";
		str += " position " + orderSite.gameObject.transform.position.ToString();
		str += ")";
		return str;
	}



}