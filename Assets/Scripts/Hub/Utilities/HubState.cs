﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreLinq;
using System.Linq;

public class HubState {
	private HierarchyHub hub;
	private GameObject commitSite;
	private HashSet<GameObject> knownSites;
	private float commitTime = 0.0f;
	public Dictionary<GameObject, List<float>> siteReports;

	public Dictionary<HierarchyHub, HubState> otherStates;


	private bool siteKnowledgeIncreased = false;

	private bool warned = false;

	public bool HasBeenWarned() {
		return warned;
	}

	public void MarkWarned() {
		warned = true;
	}


	public void MarkUnwarned() {
		warned = false;
	}

	public bool HasSiteKnowledgeIncreased() {
		return siteKnowledgeIncreased;
	}

	public void ReportSent() {
		siteKnowledgeIncreased = false;
	}


	public float GetCommitTime() {
		return commitTime;
	}

	public HubState(HierarchyHub h) {
		hub = h;
		commitSite = null;
		knownSites = new HashSet<GameObject>();
		siteReports = new Dictionary<GameObject, List<float>>();
		otherStates = new Dictionary<HierarchyHub, HubState>();
	}

	public HubState(HierarchyHub h, GameObject cs, float ct, HashSet<GameObject> ks, Dictionary<GameObject, List<float>> sr, Dictionary<HierarchyHub, HubState> os) {
		hub = h;
		commitSite = cs;
		commitTime = ct;
		knownSites = ks;
		siteReports = sr; // Change
		otherStates = os;
	}

	public void SetCommitSite(GameObject s) {
		this.commitTime = HierarchyHub.GetCurrentTime();
		this.commitSite = s;
		//hub.commitSite = s;

		if (s != null) {
			Debug.Log(hub.gameObject.transform.position.ToString() + " new site is " + s.transform.position.ToString());
			//HierarchyHub.Print("Commit " + s.GetComponent<SiteScript>(), hub.ToString());
		} else {
			Debug.Log(hub.gameObject.transform.position.ToString() + " new site is null");
			//HierarchyHub.Print("set commit site to null", hub.ToString());
		}
		

	}

	private void AddKnownSite(GameObject s) {
		knownSites.Add(s);
	}

	public void AddSiteReport(GameObject s, float estimate) {
		siteKnowledgeIncreased = true;
		//Debug.Assert(s != null);
		AddKnownSite(s);
		if (!siteReports.ContainsKey(s)) {
			siteReports.Add(s, new List<float>());
		}
		siteReports[s].Add(estimate);
		//Debug.Log("AddSiteReport - site position: " + s.transform.position.ToString() + ", estimate: " + estimate.ToString());
	}

	public float GetQualityMeanEstimateOfSite(GameObject s) {
		// Debug.Log(HierarchyHub.GetCurrentTime());
		//Debug.Assert(siteReports.ContainsKey(s));
		//Debug.Assert(s != null);
		float sum = 0.0f;
		foreach (float f in siteReports[s]) {
			sum += f;
		}
		return sum / siteReports[s].Count;
	}

	public float GetVarianceOfSiteEstimate(GameObject s) {
		float variance = 0.0f;
		float mean = GetQualityMeanEstimateOfSite(s);
		foreach (float f in siteReports[s]) {
			variance += (f - mean) * (f - mean);
		}
		return variance;
	}


	public GameObject GetCommitSite() {
		return this.commitSite;
	}

	public HashSet<GameObject> GetCopyOfKnownSites() {
		HashSet<GameObject> copy = new HashSet<GameObject>();
		foreach (GameObject s in knownSites) {
			copy.Add(s);
		}
		return copy;
	}
	public List<GameObject> GetListCopyOfKnownSites() {
		List<GameObject> copyList = new List<GameObject>();
		foreach (GameObject s in GetCopyOfKnownSites()) {
			copyList.Add(s);
		}
		return copyList;
	}

	public Dictionary<GameObject, List<float>> GetCopyOfSiteReports() {
		Dictionary<GameObject, List<float>> sr = new Dictionary<GameObject, List<float>>();
		foreach (GameObject go in knownSites) {
			sr.Add(go, new List<float>());
			foreach (float f in siteReports[go]) {
				sr[go].Add(f);
			}
		}

		return sr;
	}

	private Dictionary<HierarchyHub, HubState> GetCopyOfOtherStates() {
		Dictionary<HierarchyHub, HubState> dictionary = new Dictionary<HierarchyHub, HubState>();
		foreach (HierarchyHub hh in otherStates.Keys) {
			dictionary.Add(hh, new HubState(hh, otherStates[hh].GetCommitSite(), otherStates[hh].commitTime, null, null,null));
		}

		return dictionary;
	}

	public HubState Clone() {
		return new HubState(hub, commitSite, commitTime, GetCopyOfKnownSites(), GetCopyOfSiteReports(), GetCopyOfOtherStates());

	}

	public void AddOtherHubStateInfo(HierarchyHub h, HubState other) {
		string commitString = "null";
		if (other.GetCommitSite() != null) {
			commitString = other.GetCommitSite().transform.position.ToString();
		}
		//Debug.Log(hub.gameObject.transform.position.ToString() + " is adding " + h.gameObject.transform.position.ToString() + "'s hubstate, which is committing to: " + commitString);

		if (!otherStates.ContainsKey(h)) {
			otherStates.Add(h, other);
		} else {
			otherStates[h] = other;
		}
		
		foreach (HierarchyHub otherhub in other.otherStates.Keys) {
			if (hub == otherhub) {
				continue;
			}
			if (!this.otherStates.ContainsKey(otherhub)){
				this.otherStates.Add(otherhub, other.otherStates[otherhub]);
			}else if (other.otherStates[otherhub].commitTime > this.otherStates[otherhub].commitTime) {
				this.otherStates[otherhub] = other.otherStates[otherhub];
			}
		}

		string x = null;
		if (otherStates[h].GetCommitSite() != null) {
			x = otherStates[h].GetCommitSite().gameObject.transform.position.ToString();
		}
		//Debug.Log(hub.gameObject.transform.position.ToString() +  " perceives that the other hub is committing to site: " + x);
		foreach (GameObject go in other.knownSites) {
			this.AddSiteReport(go, other.GetQualityMeanEstimateOfSite(go));
		}
	}

	private List<HierarchyHub> GetUncommittedSubordinates() {
		List<HierarchyHub> uncommittedSubordinates = new List<HierarchyHub>();
		foreach (HierarchyHub hh in otherStates.Keys) {
			if (otherStates[hh].GetCommitSite() == null) {
				uncommittedSubordinates.Add(hh);
			}
		}
		return uncommittedSubordinates;
	}

	public List<GameObject> GetListOfUnclaimedSites() {
		List<GameObject> unclaimedSitesList = new List<GameObject>();
		foreach (GameObject s in GetUnclaimedSites()) {
			unclaimedSitesList.Add(s);
		}
		return unclaimedSitesList;
	}

	/*private List<GameObject> GetBestMUnclaimedSites(int m) {
		List<GameObject> bestMUnclaimedSites = GetListOfUnclaimedSites();
		bestMUnclaimedSites.OrderBy(s => s.GetComponent<SiteScript>().quality);

	}*/


		//TODO (HIGH): Wrong. Fix. Includes this hub's site
		//Also not recalling the actual claimed sites
	public HashSet<GameObject> GetOtherClaimedSites() {
		HashSet<GameObject> claimedSites = new HashSet<GameObject>();
		foreach (HierarchyHub hh in otherStates.Keys) {
			//Debug.Log("other hub " + hh.gameObject.transform.position.ToString() + " " + otherStates[hh].GetCommitSite().transform.position.ToString());
			claimedSites.Add(otherStates[hh].GetCommitSite());
		}
		//claimedSites.Add(this.GetCommitSite());
		claimedSites.Remove(null);
		return claimedSites;
	}
	private HashSet<GameObject> GetUnclaimedSites() {
		HashSet<GameObject> unclaimedSites = new HashSet<GameObject>();
		unclaimedSites.UnionWith(this.GetCopyOfKnownSites());
		unclaimedSites.ExceptWith(GetOtherClaimedSites());//?
		return unclaimedSites;
	}

	public GameObject GetBestUnclaimedSite() {
		if (GetUnclaimedSites().Count == 0) {
			return null;
		}
		return GetUnclaimedSites().MaxBy(x => GetQualityMeanEstimateOfSite(x));
		//return hubState.GetCopyOfKnownSites().MaxBy(x => x.GetComponent<SiteScript>().GetQuality());
	}


	public override string ToString() {
		string str = "HubState {";
		string commitSiteStr = "CommitSite : ";
		if (GetCommitSite() != null) { commitSiteStr += GetCommitSite().GetComponent<SiteScript>().ToString(); } else { commitSiteStr += "null"; }

		foreach (HierarchyHub h in otherStates.Keys) {
			commitSiteStr += "; hub " + h.transform.position.ToString() + ": ";
			if (otherStates[h].GetCommitSite() == null) {
				commitSiteStr += "null";
			} else {
				commitSiteStr += otherStates[h].GetCommitSite().transform.position.ToString();
			}
		}
		commitSiteStr += "||| ";


		string sitesStr = "SITES : ";
		foreach (GameObject s in knownSites) {
			sitesStr += s.GetComponent<SiteScript>().ToString();
		}
		sitesStr += "}";
		return str + commitSiteStr + sitesStr;
	}


}