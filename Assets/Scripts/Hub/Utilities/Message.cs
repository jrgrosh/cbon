﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Message {
	protected float sendTime;
	protected float receiveTime;
	protected HierarchyHub sender;
	protected int id;
	public static int numMessages = 0;

	public Message(HierarchyHub author) {
		id = ++numMessages;
		sender = author;
		sendTime = float.MaxValue;
		receiveTime = -1.0f;
	}

	public HierarchyHub GetSender() {
		return sender;
	}

	public void MarkSent(){
		sendTime = HierarchyHub.GetCurrentTime();
	}

	public void MarkReceived() {
		receiveTime = HierarchyHub.GetCurrentTime();
	}

	public float GetSendTime() {
		return sendTime;
	}
	public float GetReceiveTime() {
		return receiveTime;
	}

	public override string ToString() {
		string str = "Message(" + this.id.ToString() + " " + sendTime + ", ";
		if (receiveTime < 0) {
			str += "not yet received";
		} else {
			str += receiveTime;
		}
		str += ")";
		return str;
	}
}



