﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreLinq;
using System.Linq;



 public class HierarchyHub : Hub{
	public static List<HierarchyHub> hubs = new List<HierarchyHub>();


	private static bool team = true;

	private static bool none = false;
	public static void SetNone(bool non) {
		HierarchyHub.none = non;
	}

	public static bool IsTeam() {
		return team;
	}


	public static Dictionary<HierarchyHub, GameObject> optimalConfig = null;
	public bool hasBeenMessagedAboutOptimal = false;

	public static float MAX_TIME = 100.0f;
	public static int bandwidth = 45;
	public static int latency = 10;
	public static bool consensusTeam = true;
	public static int consensusCuttoff = (int)(MAX_TIME / 2);

	public static string GetOrgType() {
		if (none) {
			return "none";
		}
		if (team) {
			return "team";
		} else {
			return "hierarchy";
		}
	}

// what if belonging to another hub
	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.CompareTag("swarm_agent")) {
			this.agentsAtHub.Add(other.gameObject);
			GameObject s = other.GetComponent<UAVBehaviorScript>().site;
			if (s != null && !hubState.GetCopyOfKnownSites().Contains(s)) {
				//Print("Discover site", this.ToString());
			}
			other.gameObject.SendMessage("GiveKeys", knownKeys);
		}
	}

	void OnTriggerExit2D(Collider2D other) {
		if (other.gameObject.CompareTag("swarm_agent")) {
			this.agentsAtHub.Remove(other.gameObject);
			if (other.gameObject.GetComponent<UAVBehaviorScript>().hubLocation.x == this.gameObject.transform.position.x && other.gameObject.GetComponent<UAVBehaviorScript>().hubLocation.y == this.gameObject.transform.position.y) {
				other.gameObject.GetComponent<UAVBehaviorScript>().SetHierarchyHub(this);
			}
		}
	}
	public void SendReport(HierarchyHub recipient) {
		ReportMessage rm = new ReportMessage(this, hubState.Clone(), recipient);

		/*if (this.GetHubCommitSite() != null) {
			hubState.otherStates[recipient].MarkWarned();
		}*/
		
		recipient.GiveReport(rm);
		timeLastMsgSent = HierarchyHub.GetCurrentTime();
	}
	
	public void Start(){
		hubId = RegisterSelf(this);
		agentsAtHub = new HashSet<GameObject>();
		hubState = new HubState(this);	
		inbox = new Queue<Message>();
		outbox = new Queue<Message>();
		timeLastMsgSent = -1.0f;
		//Time.timeScale = 1.0f;
		HierarchyHub.optimalConfig = null;
		//Debug.Log("Is team: " + IsTeam().ToString());
	}

	public string GetPosition() {
		return "(" + transform.position.x + "," + transform.position.y + ")";
	}

	public GameObject commitSite = null;
	private void SelectBestUnclaimedSite() {

		GameObject bestUnclaimedSite = this.hubState.GetBestUnclaimedSite();
		if (false && this.gameObject.transform.position.x == 5 && this.gameObject.transform.position.y == 11) {
			if (bestUnclaimedSite == null) {
				Debug.Log(this.transform.position.ToString() + " best unclaimed site is null");
			} else {
				Debug.Log(this.transform.position.ToString() + " best unclaimed site is " + bestUnclaimedSite.transform.position.ToString());
			}
		}
		GameObject commitSite = this.hubState.GetCommitSite();
		if (bestUnclaimedSite != null && bestUnclaimedSite != commitSite) {
			if (commitSite == null || hubState.GetQualityMeanEstimateOfSite(commitSite) < hubState.GetQualityMeanEstimateOfSite(bestUnclaimedSite)) {
				hubState.SetCommitSite(bestUnclaimedSite);
			}
		}
	}
	public float commitSiteEstimate = -1.0f;
	public void FixedUpdate(){
		this.commitSite = hubState.GetCommitSite();
		if (this.commitSite != null && hubState.siteReports.ContainsKey(this.commitSite)) {
			this.commitSiteEstimate = hubState.GetQualityMeanEstimateOfSite(commitSite);
		} else {
			this.commitSiteEstimate = -1.0f;
		}
		if (HierarchyHub.none || isDropped) {
			SelectBestUnclaimedSite();
			if (HierarchyHub.GetCurrentTime() > MAX_TIME * 1.0f / 2.0f) {
				ManageSwarm();
			}
			return;
		}
		if (GetCurrentTime() / MAX_TIME > InformationCutoff) {
			
			if (IsTeam()) {
				TeamAlgorithm1();
			} else {
				HierarchyAlgorithm1();
			}
		}
		ManageSwarm();
		if (inbox.Count >= 1 && (GetCurrentTime() - inbox.Peek().GetSendTime() > latency)) {
			ProcessInbox();
		}
		ProcessOutbox();
	}

	
	private bool groupCommit = false;
	private bool hasSentMessages = false;

	private void TeamAlgorithm1() {
		this.SelectBestUnclaimedSite();
		if (!hasSentMessages) {
			//Debug.Log("sending reports");
			//Debug.Log("latency, bandwidth " + latency.ToString() + " " + bandwidth.ToString());
			List<HierarchyHub> sortedHubs = new List<HierarchyHub>();
			for (int i = 0; i < hubs.Count; i++) {
				HierarchyHub closest = null;
				foreach (HierarchyHub h in hubs) {
					if (sortedHubs.Contains(h)) {
						continue;
					}
					if (closest == null || Vector2.Distance(h.gameObject.transform.position, this.gameObject.transform.position) <
						Vector2.Distance(closest.gameObject.transform.position, this.gameObject.transform.position)) {
						closest = h;
					}
					sortedHubs.Add(closest);

				}
			}
			sortedHubs.Remove(this);


			foreach (HierarchyHub h in sortedHubs) {
				if (this != h) {
					outbox.Enqueue(new ReportMessage(this, this.hubState.Clone(), h));
				}
			}
			hasSentMessages = true;
		}
		//implement dud messages for dropout
		if (outbox.Count == 0 && HierarchyHub.hubs.Count > 0 && numMessagesReceived % HierarchyHub.hubs.Count - 1 == 0) {
			hasSentMessages = false;
		}
		/*
		//rejection?
		GameObject bestUnclaimedSite = this.hubState.GetBestUnclaimedSite();
		GameObject commitSite = this.hubState.GetCommitSite();
		if (bestUnclaimedSite != null) {
			if (commitSite == null || hubState.GetQualityMeanEstimateOfSite(commitSite) < hubState.GetQualityMeanEstimateOfSite(bestUnclaimedSite)) {
				hubState.SetCommitSite(bestUnclaimedSite);
			}
		}*/

	}
	private void HierarchyAlgorithm1() {

		if (hubState.GetCommitSite() == null &&
			HierarchyHub.GetCurrentTime() > HierarchyHub.InformationCutoff * 1.9f * HierarchyHub.MAX_TIME) {
			SelectBestUnclaimedSite();
		}


			if (this.IsLeader()) {
				HierarchyLeaderAlgorithm1();
			} else {
				HierarchySubordinateAlgorithm1();
			}

		if (hasBeenMessagedAboutOptimal && outbox.Count == 0 && numMessagesReceived > 0) {
			foreach (HierarchyHub hh in HierarchyHub.hubs) {
				if (hh.hasBeenMessagedAboutOptimal == false) {
					hh.hasBeenMessagedAboutOptimal = true;
					if (optimalConfig.ContainsKey(hh)){
						outbox.Enqueue(new OrderMessage(this, optimalConfig[hh], hh));
					}
					break;
				}
			}
		}

	}

	public static float InformationCutoff = .35f;
	private void HierarchySubordinateAlgorithm1() {
		if (hasSentMessages == false) {
			//Debug.Log("Leader: " + GetLeader().GetInstanceID().ToString());
			outbox.Enqueue(new ReportMessage(this, this.hubState.Clone(), GetLeader()));
			hasSentMessages = true;
		}
	}
	private void HierarchyLeaderAlgorithm1() {
		if (numMessagesReceived < hubs.Count - 1 - MapInitializer.dropNum || hasSentMessages) {
			return;
		}
		//get a list of hubs 
		//get list of sites
		//get best sites
		//assign each site to closest hub
		//issue orders, starting with hub who is farthest from site
		//Debug.Log("leader known sites and estimates: ");
		foreach (GameObject s in hubState.GetCopyOfKnownSites()) {
		//	Debug.Log("site: " + s.transform.position.ToString() + ", estimate: " + hubState.GetQualityMeanEstimateOfSite(s));
		}
		List<GameObject> bestMUnclaimedSites = hubState.GetListCopyOfKnownSites().OrderBy(s => hubState.GetQualityMeanEstimateOfSite(s)).ToList();
		bestMUnclaimedSites.RemoveRange(0, Mathf.Clamp(bestMUnclaimedSites.Count - hubs.Count, 0, bestMUnclaimedSites.Count));


		Dictionary<GameObject, HierarchyHub> siteToHubDict = new Dictionary<GameObject, HierarchyHub>();

		//List<GameObject> siteList = new List<GameObject>();
		List<HierarchyHub> hubList = new List<HierarchyHub>();
		foreach (HierarchyHub h in hubs) {
			hubList.Add(h);
		}
		for (int i = 0; i < bestMUnclaimedSites.Count; i++) {
			GameObject site = bestMUnclaimedSites[i];
			HierarchyHub hub = hubList.MinBy(h => Vector2.Distance(site.transform.position, h.transform.position));
			siteToHubDict.Add(site, hub);
			hubList.Remove(hub);
		}

		Debug.Log("bestMUnclaimedSites");
		foreach (GameObject s in bestMUnclaimedSites) {
			Debug.Log(s.transform.position.ToString());
		}

		Debug.Log("Pairings");
		foreach (GameObject s in siteToHubDict.Keys) {
			Debug.Log("pairing - site: " + s.transform.position.ToString() + ", hub:" + siteToHubDict[s].transform.position.ToString()); 
		}

		if (optimalConfig == null) {
			optimalConfig = new Dictionary<HierarchyHub, GameObject>();
			foreach (GameObject key in siteToHubDict.Keys) {
				optimalConfig[siteToHubDict[key]] = key;
			}
		}
		if (optimalConfig.ContainsKey(this)) {
			this.hubState.SetCommitSite(optimalConfig[this]);
		}
		hasBeenMessagedAboutOptimal = true;


		hasSentMessages = true;


	}

	public static float GetCurrentTime() {
		return Time.time - ExperimentRunner.startTime;
	}
	public static void Print(string what, string who) {
		//Debug.Log(who + " did " + what);
	}
	public HubState GetHubState() {
		return hubState.Clone();
	}
	public static void SetTeamStatus(bool status) {
		HierarchyHub.team = status;
		Debug.Log(HierarchyHub.GetOrgType());
	}
	public static void SetConsensusTeamStatus(bool consensusTeamStatus) {
		HierarchyHub.consensusTeam = consensusTeamStatus;

	}
	//public bool IsTeam() {
	//	return team;
	//}
	public override string ToString() {
		string rank;
		if (this.IsLeader()) { rank = "Leader"; } else { rank = "Subordinate"; }
		return "Hub(" + transform.position.x + ", " + transform.position.y + ") - " + rank + " - " + this.hubState.ToString();

	}
	public static void SetLatencyAndBandwdith(int l, int b) {
		latency = l;
		bandwidth = b;
		Debug.Log("Latency, bandwidth " + l.ToString() + ", " + b.ToString());
	}
	private static int RegisterSelf(HierarchyHub hub){
		if (hubs.Count >= MapInitializer.numHubs) {
			hubs.Clear();
		}
		hubs.Add(hub);
		return (hubs.Count-1);
	}
	private HierarchyHub GetLeader(){
		if (hubs.Count == 0) {
			return null;
		} else {
			return hubs[0];
		}
	}
	private bool IsLeader(){
		return(this == this.GetLeader());
	}

	/*
	private int GetRandomIndex(int upperBound) {
		return (int)(UnityEngine.Random.value * upperBound);
	}
	
	private void Team_RandAlgorithm() {
		if (GetHubCommitSite() != null && GetUnwarnedHubs().Count > 0) {
			List<HierarchyHub> unwarnedNeighbors = GetUnwarnedHubs();
			HierarchyHub unwarnedNeighbor = unwarnedNeighbors[GetRandomIndex(unwarnedNeighbors.Count)];
			SendReport(unwarnedNeighbor);
		} else {
			int index = GetRandomIndex(GetNeighbors().Count);
			//Debug.Log(index.ToString() + ", " + GetNeighbors().Count.ToString());
			SendReport(GetNeighbors()[index]);
		}
	}

	private void Hierarchy_RandAlgorithm() {
		if (IsLeader()) {
			if (GetUnclaimedSites().Count > 0 && GetUncommittedSubordinates().Count > 0) {
				List<GameObject> unclaimedSitesList = GetListOfUnclaimedSites();
				List<HierarchyHub> uncommittedSubordinates = GetUncommittedSubordinates();
				GameObject s = unclaimedSitesList[GetRandomIndex(unclaimedSitesList.Count)];
				HierarchyHub hh = uncommittedSubordinates[GetRandomIndex(uncommittedSubordinates.Count)];
				SendOrder(hh, s);
			}
			
		} else {
			SendReport(GetLeader());
		}
	}

	private void Hierarchy_Algorithm2() {
		if (IsLeader()) {
			List<GameObject> unclaimedSites = GetListOfUnclaimedSites();
			List<HierarchyHub> uncommittedSubordinates = GetUncommittedSubordinates();

			GameObject bestUnclaimedSite = GetBestUnclaimedSite();
			if (bestUnclaimedSite == null || uncommittedSubordinates.Count == 0) {
				return;
			} else if (uncommittedSubordinates.Count == 1 || unclaimedSites.Count == 0) {
				SendOrder(GetUncommittedSubordinates().MinBy(sub => Vector2.Distance(sub.transform.position, bestUnclaimedSite.transform.position)), bestUnclaimedSite);
			} else {
				List<HierarchyHub> sortedUncommittedSubs = GetUncommittedSubordinates().OrderBy(sub => Vector2.Distance(sub.transform.position, bestUnclaimedSite.transform.position)).ToList();
				SendWarning(sortedUncommittedSubs[1], bestUnclaimedSite);
			}
		} else {
			if (hubState.HasSiteKnowledgeIncreased()) {
				SendReport(GetLeader());
				hubState.ReportSent();
			}
		}
	}	
	private void TeamReportFixedUpdate() {
		if (GetNeighbors().Count == 0) {
			//Print("Count 0", this.ToString());
			return;
		}

		int num_neighbors = GetNeighbors().Count;
		int rand_index = (int)((UnityEngine.Random.value)*(num_neighbors));
		SendReport(GetNeighbors()[rand_index]);
	}*/
}
	 
	 
	 
	 
	 
