﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreLinq;
using System.Linq;

public class Hub : MonoBehaviour {
	protected Queue<Message> inbox;
	protected Queue<Message> outbox;
	protected float timeLastMsgSent;
	protected HashSet<GameObject> agentsAtHub;
	protected HubState hubState;
	protected int hubId;

	public bool isDropped = false;

	public void Drop() {
		isDropped = true;
	}



	protected int numMessagesReceived = 0;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	private List<GameObject> agents = new List<GameObject>();
	public void SelfRegisterAgent(GameObject agent) {
		agents.Add(agent);
	}
	/*private List<HierarchyHub> GetNeighbors() {
		List<HierarchyHub> neighbors = new List<HierarchyHub>();
		foreach (HierarchyHub h in hubs) {
			if (this != h) {
				neighbors.Add(h);
			}
		}
		return neighbors;
	}*/
	/*public void SendReport(HierarchyHub recipient) {
		ReportMessage rm = new ReportMessage(this, hubState.Clone());

		if (this.GetHubCommitSite() != null) {
			hubState.otherStates[recipient].MarkWarned();
		}
		recipient.GiveReport(rm);
		timeLastMsgSent = HierarchyHub.GetCurrentTime();
	}*/

	private void ProcessReport(ReportMessage reportMessage) {
		//Debug.Log(this.transform.position.ToString() + " is processing report");
		Debug.Log(reportMessage.ToString());

		HierarchyHub sender = reportMessage.GetSender();

		hubState.AddOtherHubStateInfo(sender, reportMessage.GetReportedState());
		

		if (HierarchyHub.IsTeam()) {
			GameObject cs = this.hubState.GetCommitSite();
			if (cs!= null &&(hubState.otherStates[sender].GetCommitSite() == hubState.GetCommitSite())) {
				Debug.Log(this.gameObject.transform.position.ToString() + " has same cs as " + sender.transform.position.ToString() + " : " + cs.transform.position.ToString());
				Debug.Log(Vector2.Distance(cs.transform.position, sender.transform.position).ToString() + " " + Vector2.Distance(cs.transform.position, this.transform.position).ToString());
				if(reportMessage.GetReportedState().GetCommitTime() < this.hubState.GetCommitTime() ||
					(
						(Vector2.Distance(cs.transform.position, sender.transform.position) < Vector2.Distance(cs.transform.position, this.transform.position))
							&&
						(reportMessage.GetReportedState().GetCommitTime() == this.hubState.GetCommitTime())
					)
				){
				//if (Vector2.Distance(cs.transform.position, sender.transform.position) < Vector2.Distance(cs.transform.position, this.transform.position)) {
					Debug.Log("route a");
					hubState.SetCommitSite(null);
					
				} else {
					Debug.Log("route b");
					hubState.otherStates[sender].SetCommitSite(null);
				}
			}
			//Debug.Log(this.transform.position.ToString() + "recognizes the claimed sites of other hubs:");
			foreach (GameObject s in this.hubState.GetOtherClaimedSites()) {
				//Debug.Log(s.transform.position.ToString());
			}
			Debug.Log("message received: " + sender.transform.position.ToString() + " " + this.transform.position.ToString());
			Debug.Log(this.hubState.ToString());
		}
	}


	public void ProcessInbox() {
		if (isDropped) {
			return;
		}
		//Debug.Log(messages.Count);
		//Debug.Log("latency: " + latency);
		Message m = inbox.Dequeue();
	
		m.MarkReceived();
		numMessagesReceived++;
		//Debug.Log(m.ToString());
		//Debug.Log( m.ToString());
		//HierarchyHub.Print("Received " + m.GetType(), this.ToString());
		if (m.GetType() == typeof(OrderMessage)) {
			ProcessOrder((OrderMessage)(m));
		} else if (m.GetType() == typeof(ReportMessage)) {
			ProcessReport((ReportMessage)(m));
		} else {
			throw new System.Exception("Invalid message type");
		}
		//Debug.Log(this.ToString() + " has new hubstate: " + hubState.ToString());
		//}
	}

	public void ProcessOutbox() {
		if (isDropped) {
			return;
		}
		if (outbox.Count > 0 && (HierarchyHub.GetCurrentTime() - timeLastMsgSent > HierarchyHub.bandwidth)) {
			timeLastMsgSent = HierarchyHub.GetCurrentTime();
			Message m = outbox.Dequeue();
			m.MarkSent();
			if (m.GetType() == typeof(OrderMessage)) {
				OrderMessage om = (OrderMessage)(m);
				om.recipient.inbox.Enqueue(om);
			} else if (m.GetType() == typeof(ReportMessage)) {
				ReportMessage rm = (ReportMessage)(m);
				rm.SetHubState(this.hubState);
				rm.recipient.inbox.Enqueue(rm);
			}
			//Debug.Log(m.ToString());
		}
	}

	//TODO: how does Hub know who neighbors are? how are leaders selected?
	protected HashSet<GameObject> knownKeys = new HashSet<GameObject>();
	private int resourceUses = 0;
	private void UseResource(GameObject s) {
		knownKeys.Add(s);
		resourceUses++;
	}

	private GameObject forbidSite = null;
	protected void ManageSwarm() {
		GameObject swarmActualCommit = GetActualSwarmCommitAtHub();
		GameObject hubChosenSite = GetHubCommitSite();

		string scs = "null";
		if (swarmActualCommit != null) {
			scs = swarmActualCommit.transform.position.ToString();
		}
		//Debug.Log(this.gameObject.transform.position.ToString() + "'s swarm is committing to " + scs);

		if ((hubState.GetOtherClaimedSites().Contains(swarmActualCommit) || 
		     (hubChosenSite != null && hubChosenSite != swarmActualCommit && swarmActualCommit != null && 
			  ((!HierarchyHub.IsTeam() && HierarchyHub.GetCurrentTime() > HierarchyHub.InformationCutoff *  HierarchyHub.MAX_TIME) || (HierarchyHub.IsTeam() && ( HierarchyHub.GetCurrentTime() > HierarchyHub.InformationCutoff * 1.9f * HierarchyHub.MAX_TIME)))
			 )
			) 
			&& forbidSite == null
		   ) {
					//Debug.Log(this.gameObject.transform.position.ToString() + " " + (hubChosenSite != null && swarmActualCommit != null && swarmActualCommit != hubChosenSite).ToString() + " " + hubState.GetOtherClaimedSites().Contains(swarmActualCommit).ToString());
					forbidSite = swarmActualCommit;
					//ForbidSite(swarmActualCommit);
		}

		if (forbidSite != null && forbidSite != hubChosenSite) {
			ForbidSite(forbidSite);
		}

		
		if (hubChosenSite != null) {
			PromoteSite(hubChosenSite);
		}
		/*
		if (hubChosenSite != null) {
			if (swarmActualCommit == null) {
				PromoteSite(hubChosenSite);
			} else if (swarmActualCommit != hubChosenSite) {
				ForbidSite(swarmActualCommit);
			}
		}*/
	}
	public void GiveOrder(OrderMessage orderMessage) {
		inbox.Enqueue(orderMessage);
		//Debug.Log("sending " + orderMessage.ToString());
	}

	private void ProcessOrder(OrderMessage orderMessage) {
		hubState.SetCommitSite(orderMessage.GetOrderSite());
		Debug.Log(orderMessage.ToString());
		//Debug.Log(transform.position.ToString() + " received order for site " + orderMessage.GetOrderSite().GetInstanceID() + " " + hubState.GetCommitSite().GetInstanceID());
	}

	public void GiveReport(ReportMessage reportMessage) {
		inbox.Enqueue(reportMessage);
		//Debug.Log("sending " + reportMessage.ToString());
	}

	private float GetMaxScoreThreshold() {
		return (HierarchyHub.MAX_TIME - HierarchyHub.GetCurrentTime()) / HierarchyHub.MAX_TIME;
	}

	public GameObject GetHubCommitSite() {
		return hubState.GetCommitSite();
	}

	GameObject lastPromoteSite = null;
	private void PromoteSite(GameObject s) {
		if (lastPromoteSite != s) {
			string xyz = "null";
			if (s != null) {
				xyz = s.transform.position.ToString();
			}
			lastPromoteSite = s;
			Debug.Log(this.gameObject.transform.position.ToString() + " promote " + xyz);
		}
		foreach (GameObject agent in agentsAtHub) {
			agent.SendMessage("PromoteSite", s);
		}
	}

	private void InhibitSite(GameObject s) {
		foreach (GameObject agent in agentsAtHub) {
			agent.SendMessage("InhibitSite", s);
		}
	}

	GameObject lastForbidSite = null;
	private void ForbidSite(GameObject s) {
		//foreach (GameObject agent in agentsAtHub) {
		//Debug.Log(this.gameObject.transform.position.ToString() + " FORBIDDING " + s.transform.position.ToString());
		if (s!=null &&(lastForbidSite!=s)) {
			lastForbidSite = s;
			Debug.Log(this.gameObject.transform.position.ToString() + " FORBIDDING " + s.transform.position.ToString());
		} 
		foreach (GameObject agent in agents){
			agent.SendMessage("ForbidSite", s);
		}
	}
	public int GetCommitSiteId() {
		if (hubState.GetCommitSite() == null) {
			return -1;
		} else {
			return hubState.GetCommitSite().GetComponent<SiteScript>().GetSiteId();
		}
	}

	public void TEST_SetHubState(HubState hs) {
		hubState = hs;
	}

	public void ReportSite(GameObject s, float estimate) {
		hubState.AddSiteReport(s, estimate);
	}
	public int GetActualSwarmCommitId() {
		GameObject actualSwarmCommitSite = GetActualSwarmCommit();
		if (actualSwarmCommitSite == null) {
			return -1;
		} else {
			return actualSwarmCommitSite.GetComponent<SiteScript>().site_id;
		}
	}
	public GameObject GetActualSwarmCommit() {
		Dictionary<GameObject, int> siteCounts = new Dictionary<GameObject, int>();
		foreach (GameObject agent in agents) {
			GameObject site = agent.GetComponent<UAVBehaviorScript>().site;
			if (!siteCounts.ContainsKey(site)) {
				siteCounts.Add(site, 1);
			} else {
				siteCounts[site] += 1;
			}
		}
		List<GameObject> sitesOfSwarm = new List<GameObject>();
		string str = "";
		foreach (GameObject s in siteCounts.Keys) {
			sitesOfSwarm.Add(s);
			if (s == null) {
				str += "null";
			}else{
				str += s.transform.position.ToString() + " " + s.GetComponent<SiteScript>().SampleQuality(true);
			}
			str += " " + siteCounts[s].ToString() + "; ";
		}
		Debug.Log(this.gameObject.transform.position.ToString() + " " + str);
		
		if (sitesOfSwarm.Count == 0) {
			return null;
		}
		GameObject mostPopularSite = sitesOfSwarm.MaxBy(x => siteCounts[x]);
		if (siteCounts[mostPopularSite] > .5f * agents.Count) {
			return mostPopularSite;
		} else {
			return null;
		}

	}
	public GameObject GetActualSwarmCommitAtHub() {
		Dictionary<GameObject, int> siteCounts = new Dictionary<GameObject, int>();
		foreach (GameObject agent in agentsAtHub) {
			GameObject site = agent.GetComponent<UAVBehaviorScript>().site;
			if (!siteCounts.ContainsKey(site)) {
				siteCounts.Add(site, 1);
			} else {
				siteCounts[site] += 1;
			}
		}
		List<GameObject> sitesOfSwarm = new List<GameObject>();
		string str = "";
		foreach (GameObject s in siteCounts.Keys) {
			sitesOfSwarm.Add(s);
			if (s == null) {
				str += "null";
			} else {
				str += s.transform.position.ToString();
			}
			str += " " + siteCounts[s].ToString() + "; ";
		}
		//Debug.Log(this.gameObject.transform.position.ToString() + " " + str);
		if (sitesOfSwarm.Count == 0) {
			//Debug.Log("yup no should be here");
			return null;
		}
		//Debug.Log("agent count " + agents.Count.ToString());
		GameObject mostPopularSite = sitesOfSwarm.MaxBy(x => siteCounts[x]);
		if (mostPopularSite == null) {
			//Debug.Log("nooooooooo");
		} else {
			//Debug.Log(mostPopularSite.transform.position.ToString());
		}
		if (siteCounts[mostPopularSite] > .5f * agents.Count) {
			return mostPopularSite;
		} else {
			return null;
		}

	}


}
