﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SiteTime {
	public GameObject site;
	public float time;

	public SiteTime(GameObject s, float t) {
		this.site = s;
		this.time = t;
	}

}


public class SiteIntroductionManager  {
	/*
	private Dictionary<HierarchyHub, List<UAVBehaviorScript>> hubLocsToAgents;// = new Dictionary<Vector2, List<UAVBehaviorScript>>();
	private Queue<SiteTime> queue;// = new Queue<SiteTime>();

	// Use this for initialization
	public SiteIntroductionManager () {
		hubLocsToAgents = new Dictionary<HierarchyHub, List<UAVBehaviorScript>>();
		queue = new Queue<SiteTime>();
	}

	public void AddHubAndAgentPairing(GameObject hub, GameObject agent) {
		HierarchyHub hubScript = hub.GetComponent<HierarchyHub>();
		if (!hubLocsToAgents.ContainsKey(hubScript)) {
			hubLocsToAgents.Add(hubScript, new List<UAVBehaviorScript>());
		}
		UAVBehaviorScript uavBs = agent.GetComponent<UAVBehaviorScript>();
		hubLocsToAgents[hubScript].Add(uavBs);
	}

	public void AddSite(GameObject s) {
		SiteTime st = new SiteTime(s, 10);
		queue.Enqueue(st);
	}

	private UAVBehaviorScript SelectAgent() {
		HierarchyHub hubScript = SelectHub();
		foreach (UAVBehaviorScript uavBs in hubLocsToAgents[hubScript]) {
			if (uavBs.site == null) {
				return uavBs;
			}
		}
		throw new System.Exception("No agents available for selection.");
	}

	private HierarchyHub SelectHub() {
		int index = (int)(UnityEngine.Random.value * hubLocsToAgents.Keys.Count);
		List<HierarchyHub> keyList = new List<HierarchyHub>(hubLocsToAgents.Keys);
		return keyList[index];
	}


	
	// Update is called once per frame
	public void FixedUpdate () {
		
		if (queue.Count == 0) {
			//Debug.Log("Count == 0");
			return;
		} else {
			//Debug.Log((GameManagerScript.GetCurrentGameTime().ToString()) + " " + (queue.Peek().time.ToString()));
			if (GameManagerScript.GetCurrentGameTime() > queue.Peek().time) {
				//Debug.Log("SiteTime dequeued for agent");
				this.SelectAgent().SetSite(queue.Dequeue().site);
			}
		}
	}*/
}
