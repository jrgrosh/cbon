﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;

public class MapInitializer : MonoBehaviour {
	public GameObject hubPrefab;
	public GameObject sitePrefab;
	public GameObject agentPrefab;
	private static XmlDocument doc;


	void InstantiateSite(Vector3 position, float quality) {
		GameObject site = Instantiate(sitePrefab, position, new Quaternion());
		//site.GetComponent<SiteScript>().quality = quality;
		site.GetComponent<SiteScript>().a = quality;
	}

	void InstantiateUniformlyDistributedSites(float max_width, float max_height, int n) {
		for (int i = 0; i < n; i++) {
			InstantiateSite( new Vector3(2*(.5f - Random.value) * max_width, 2 * (.5f - Random.value) * max_height), Random.value);
		}
	}

	void InstantiateMultiModalDistributedSites(float max_width, float max_height, int k, int num_modes) {//Rough. TODO: make less... uh. rough.
		List<Vector3> modes = new List<Vector3>();
		for (int i = 0; i < num_modes; i++) {
			modes.Add(new Vector3(2 * (.5f - Random.value) * max_width, 2 * (.5f - Random.value) * max_height));
		}

		for (int i = 0; i < k; i++) {
			//Debug.Log(i % modes.Count);
			//Debug.Log(modes[i % modes.Count]);
			InstantiateSite(new Vector3(modes[i%modes.Count].x + 3*Random.value, modes[i % modes.Count].y + 3*Random.value), Random.value);
		}
	}


	void InstantiateHubAndSwarm(Vector3 hubPosition, int k, bool team, bool consensusTeam, bool none,bool dropped) {
		GameObject hub = Instantiate(hubPrefab, hubPosition, new Quaternion());
		HierarchyHub.SetTeamStatus(team);
		HierarchyHub.SetConsensusTeamStatus(consensusTeam);
		HierarchyHub.SetNone(none);
		if (dropped) {
			hub.GetComponent<HierarchyHub>().Drop();
		}
		for (int i = 0; i < k; i++) {
			GameObject agent = InstantiateAgent(hubPosition, hubPosition);
		}
	}

	GameObject InstantiateAgent(Vector3 position, Vector3 hubLocation) {
		GameObject agent = Instantiate(agentPrefab, position, new Quaternion());
		agent.GetComponent<UAVBehaviorScript>().hubLocation = hubLocation;
		return agent;
	}

	[RuntimeInitializeOnLoadMethod]
	static void OnRuntimeMethodLoad() {
		Application.runInBackground = true;
		doc = new XmlDocument();
		doc.Load("init.xml");
	}

	// Use this for initialization
	void Start () {
			LoadCurrentTrial();
	}

	/*void InstantiateHubsAndSwarms(int numHubs, int numRobotsPerHub, string hubDistribution, bool team, bool consensusTeamStatus) {
		for (int i = 0; i < numHubs; i++) {
			float x = (.5f - UnityEngine.Random.value) * 25;
			float y = (.5f - UnityEngine.Random.value) * 25;
			Vector2 position = new Vector2(x, y);
			InstantiateHubAndSwarm(position, numRobotsPerHub, team, consensusTeamStatus);
		}
	}*/

	void InstantiateSites(int numSites, string siteDistribution) {
		for (int i = 0; i < numSites; i++) {
			float x = (.5f - UnityEngine.Random.value) * 25;
			float y = (.5f - UnityEngine.Random.value) * 25;
			Vector2 position = new Vector2(x, y);
			float quality = UnityEngine.Random.value;
			InstantiateSite(position, quality);
		}
	}

	/*//void InitializeExperiment(int numHubs, int numRobotsPerHub, int numSites, bool team, string siteDistribution, string hubDistribution) {
		//InstantiateHubsAndSwarms(numHubs, numRobotsPerHub, hubDistribution, team);
		//InstantiateSites(numSites, siteDistribution);
	//	LoadXML();	

	//}*/
	public static int numHubs;
	public static string hubDistribution;
	public static string siteDistribution;
	public static int dropNum = -1;

	void LoadCurrentTrial() {
		XmlDocument doc = new XmlDocument();
		doc.Load("init.xml");
		XmlNode currentTrial = doc.GetElementsByTagName("trial")[current_trial_id];
		XmlNodeList hubs = currentTrial["hubs"].GetElementsByTagName("hub");
		string orgType = currentTrial["orgType"].InnerText;
		float latency = float.Parse(currentTrial["latency"].InnerText);
		float bandwidth = float.Parse(currentTrial["bandwidth"].InnerText);
		HierarchyHub.SetLatencyAndBandwdith((int)(latency * (HierarchyHub.MAX_TIME*(1 - .35f))), (int)(bandwidth * (HierarchyHub.MAX_TIME * (1 - .35f))));

		numHubs = int.Parse(currentTrial["numHubs"].InnerText);
		hubDistribution = currentTrial["hubDistribution"].InnerText;
		siteDistribution = currentTrial["siteDistribution"].InnerText;
		float sigma = float.Parse(currentTrial["siteNoiseStd"].InnerText);
		int numDropped = int.Parse(currentTrial["numDropped"].InnerText);
		dropNum = numDropped;
		SiteScript.a_std = sigma;

		bool team;
		bool none = false;
		if (orgType == "team") {
			team = true;
		} else if (orgType == "hierarchy") {
			team = false;
		} else if (orgType == "none") {
			team = false;
			none = true;
		} else {
			throw new System.Exception("invalid value for orgType");
		}
		//Debug.Log(team);


		bool consensusTeamStatus = bool.Parse(currentTrial["consensusTeamStatus"].InnerText);
		int i = 0; // you see nothing
		foreach (XmlNode hub in hubs) {
			//Debug.Log("Hub: " + hub["x"].InnerText + " " + hub["y"].InnerText);
			float x = float.Parse(hub["x"].InnerText);
			float y = float.Parse(hub["y"].InnerText);
			//bool team// = true;// bool.Parse(hub["team"].InnerText);
			int numSwarmAgents = int.Parse(hub["numSwarmAgents"].InnerText);
			bool dropped = false;
			if (numDropped > 0 && i > 0) {
				dropped = true;
				numDropped--;
			}
			InstantiateHubAndSwarm(new Vector2(x, y), numSwarmAgents, team, consensusTeamStatus, none, dropped);
			i++;
		}
		XmlNodeList sites = currentTrial["sites"].GetElementsByTagName("site");
		foreach (XmlNode site in sites) {
			//Debug.Log("Site: " + site["x"].InnerText + " " + site["y"].InnerText);
			float x = float.Parse(site["x"].InnerText);
			float y = float.Parse(site["y"].InnerText);
			float q = float.Parse(site["q"].InnerText);
			//Debug.Log("Site: " + site["x"].InnerText + " " + site["y"].InnerText + " " + q);
			InstantiateSite(new Vector2(x, y), q);

		}
		current_trial_id++;
	}
	/*
	void LoadXML() {
		XmlDocument doc = new XmlDocument();
		doc.Load("init.xml");
		XmlNodeList hubs = doc.GetElementsByTagName("hub");
		foreach (XmlNode hub in hubs) {
			//Debug.Log("Hub: "+ hub["x"].InnerText + " " + hub["y"].InnerText);
			float x = float.Parse(hub["x"].InnerText);
			float y = float.Parse(hub["y"].InnerText);
			bool team = bool.Parse(hub["team"].InnerText);
			int numSwarmAgents = int.Parse(hub["numSwarmAgents"].InnerText);
			InstantiateHubAndSwarm(new Vector2(x, y), numSwarmAgents, team);
		}
		XmlNodeList sites = doc.GetElementsByTagName("site");
		foreach (XmlNode site in sites) {
			//Debug.Log("Site: " + site["x"].InnerText + " " + site["y"].InnerText);
			float x = float.Parse(site["x"].InnerText);
			float y = float.Parse(site["y"].InnerText);
			float q = float.Parse(site["q"].InnerText);
			//Debug.Log("Site: " + site["x"].InnerText + " " + site["y"].InnerText +  " " + q);
			InstantiateSite(new Vector2(x, y), q);

		}
	}*/
	public static int current_trial_id = 0;

// Update is called once per frame
void Update () {
		
	}
}
