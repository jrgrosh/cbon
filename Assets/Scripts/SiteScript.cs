﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SiteScript : MonoBehaviour {

	public float a;
	public static float a_std = -1.0f;

	public float b;
	public float b_std;

	/*public float GetEstimate(bool hasKey) {
		if (hasKey) {
			return a + b;
		} else {
			return RandomFromDistribution.RandomNormalDistribution(a, a_std) + RandomFromDistribution.RandomNormalDistribution(b, b_std);
		}
	}*/
	
	//public float quality = 1.0f;
	//public static float sigma = .3f;
	public int site_id = -1;

	private static int site_count = 0;


	public static List<SiteScript> sites = new List<SiteScript>();

	private static int SelfRegisterSite(SiteScript s) {
		sites.Add(s);
		return site_count++;
	}

	public int GetSiteId() {
		return site_id;
	}

	// Use this for initialization
	void Start () {
		/*a = UnityEngine.Random.value;
		b = UnityEngine.Random.value;
		a_std = UnityEngine.Random.value;
		b_std = UnityEngine.Random.value;*/

		this.site_id = SiteScript.SelfRegisterSite(this);
        //MessagingFramework.RegisterSite(this); //TODO: Uncomment
        //quality = Random.value;
        //Animator gameObject.GetComponent<Animator>;
        //Quality of Hub graphically shown by each additional decimal moving up
        //through colors of the rainbow/ 0.0x = black, 0.2x = red, 0.3x = orange etc.
        Renderer rend = gameObject.GetComponent<Renderer>();
        Color siteColor = new Color();
		//Or one that just assigns a color by first decimal place
		int tenths = (int) (a * 10);
        switch (tenths){
            case 0:
                siteColor = Color.black;
                break;
            case 1:
                siteColor = Color.gray;
                break;
            case 2:
                siteColor = Color.red;
                break;
            case 3:
                siteColor =  new Color(1, 0.5f, 0, 1); //orange
                break;
            case 4:
                siteColor = Color.yellow;
                break;
            case 5:
                siteColor = Color.green;
                break;
            case 6:
                siteColor = Color.cyan;
                break;
            case 7:
                siteColor = Color.blue;
                break;
            case 8:
                siteColor = Color.magenta;
                break;
            default:
                siteColor = Color.white;
                break;

        }

        rend.material.SetColor("_Color", siteColor);
    }

	//TODO: change to GetPosition method in abstract class for sites and UAVS/swarm agents
	public Vector2 GetCurrentPosition(){
		return new Vector2 (transform.position.x, transform.position.y);
	}

	/*public float GetQuality(){
		return quality;
	}*/

	public float SampleQuality(bool hasKey) {
		//float offset = (.5f - UnityEngine.Random.value) / 4.0f;
		//return Mathf.Clamp(GetQuality() + offset, 0, 1);
		if (hasKey) {
			return a + b;
		} else {
			return generateGuassianNoise(a, a_std);
		}
	}

	// Update is called once per frame
	void Update () {
		
	}

	public string toPos(){
		return "Site at (" + transform.position.x + ", " + transform.position.y + ")";
	}

	public override string ToString() {
		return "Site at (" + transform.position.x + ", " + transform.position.y + "), quality: " + this.a + "; ";
	}

	public static float generateGuassianNoise(float mu, float sigma){
		//Debug.Log("mu : " + mu.ToString() + " sigma: " + sigma.ToString());
		float u1 = 1.0f - Random.value;
		float u2 = 1.0f - Random.value;
		float randStdNormal = Mathf.Sqrt(-2.0f * Mathf.Log(u1)) * Mathf.Sin(2.0f * Mathf.PI * u2);
		//Debug.Log("randStdNormal: " + randStdNormal.ToString());
		float randNormal = mu + sigma * randStdNormal;
		return randNormal;
	}
}
