﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System.Data;
using Mono.Data.Sqlite;

/*
 * Measuring effective knowledge is hard - we can measure what has been known at a certain cutoff time and how that knowledge is shared after,
 * but if sites are discovered close enough to the end of the simulation, the org should not be expected to adjust the entire solution according to that new knowledge
 */


public class TestInstance {
	private int latency;
	private int bandwidth;
	private int test_instance_id;
	private string orgType;

	private static int test_instance_count = 0;
	private static int GetAssignedTestInstanceId() {
		return test_instance_count++;
	}

	public TestInstance(int l, int b, bool team){
		test_instance_id = GetAssignedTestInstanceId();
		latency = l;
		bandwidth = b;
		
		if (team) {
			orgType = "Team";
		} else {
			orgType = "Hierarchy";
		}
	}

	public int GetTestInstanceId() {
		return test_instance_id;
	}

	public int GetLatency() {
		return latency;
	}

	public int GetBandwidth() {
		return bandwidth;
	}

	public void InitOrgType() {
		if (orgType == "Team") {
			HierarchyHub.SetTeamStatus(true);
		} else if (orgType == "Hierarchy") {
			HierarchyHub.SetTeamStatus(false);
		} else {
			throw new System.Exception("uh shouldn't be here");
		}
		
	}

	public string GetOrgType() {
		return orgType;
	}

}

public class ExperimentRunner : MonoBehaviour {
	private static string dbPath;
	private static Queue<TestInstance> tests;
	private TestInstance test;
	private bool lockout = false;
	public static float startTime = 0.0f;
	static bool has_restarted = false;

    //adjust timescale
    

	static Queue<TestInstance> GenerateTests() {
		return null;
	}

	[RuntimeInitializeOnLoadMethod]
	static void OnRuntimeMethodLoad() {
		dbPath = "URI=file:" + Application.persistentDataPath + "/exampleDatabase.db";
		ExperimentRunner.ClearDatabase();
		ExperimentRunner.CreateSchema();

		tests = GenerateTests();// new Queue<TestInstance>();
		//tests.Enqueue(new TestInstance(10, 45));
		//tests.Enqueue(new TestInstance(15, 45));
		//tests.Enqueue(new TestInstance(20, 45));
	}

	static void DoCommand(string cmd_text, string description) {
		using (var conn = new SqliteConnection(dbPath)) {
			conn.Open();
			using (var cmd = conn.CreateCommand()) {
				cmd.CommandType = CommandType.Text;
				cmd.CommandText = cmd_text;
				var result = cmd.ExecuteNonQuery();
				//Debug.Log(description + " - " + result);
			}
		}
	}

	private static void ClearDatabase() {
		DoCommand("DROP TABLE IF EXISTS Hub;", "Drop Hub Table");
		DoCommand("DROP TABLE IF EXISTS Site;", "Drop Site Table");
		DoCommand("DROP TABLE IF EXISTS Trial;", "Drop Trial Table");
		DoCommand("DROP TABLE IF EXISTS SiteKnownByHub;", "Drop SiteKnownByHub Table");
	}

	static void CreateSchema() {
		string cmd_text = "CREATE TABLE IF NOT EXISTS 'Trial' (" +
								  " 'trial_id' INTEGER PRIMARY KEY, " +
								  " 'time' TEXT NOT NULL, " +
								  " 'latency' INTEGER NOT NULL, " +
								  " 'bandwidth' INTEGER NOT NULL," +
								  " 'orgType' TEXT NOT NULL, " +
								  " 'consensusType' TEXT NOT NULL, " + 
								  " 'hubDistribution' TEXT NOT NULL, " +
								  " 'siteDistribution' TEXT NOT NULL," +
								  " 'siteNoise' TEXT NOT NULL," +
								  " 'numDropped' TEXT NOT NULL" +
								  ");";
		DoCommand(cmd_text, "CREATE Trial Table: ");

		cmd_text = "CREATE TABLE IF NOT EXISTS 'Site' (" +
								  " 'site_id' INTEGER PRIMARY KEY, " +
								  " 'location' TEXT NOT NULL, " +
								  " 'quality' INTEGER NOT NULL, " +
								  " 'f_trial_id' INTEGER NOT NULL," +
								  " FOREIGN KEY(f_trial_id) REFERENCES Trial(trial_id)" +
								  ");";
		DoCommand(cmd_text, "CREATE Site Table: ");

		cmd_text = "CREATE TABLE IF NOT EXISTS 'Hub' (" +
								  " 'hub_id' INTEGER PRIMARY KEY AUTOINCREMENT, " +
								  " 'location' TEXT NOT NULL, " +
								  " 'chosen_site_id' INTEGER, " +
								  " 'actual_site_id' INTEGER, " +
								  " 'f_trial_id' INTEGER NOT NULL," +
								  " FOREIGN KEY(chosen_site_id) REFERENCES Site(site_id)," +
								  " FOREIGN KEY(actual_site_id) REFERENCES Site(site_id)," + 
								  " FOREIGN KEY(f_trial_id) REFERENCES Trial(trial_id)" +
								  ");";

		DoCommand(cmd_text, "CREATE Hub Table:");

		cmd_text = "CREATE TABLE IF NOT EXISTS 'SiteKnownByHub' (" +
					"'site_hub_link_id' INTEGER PRIMARY KEY AUTOINCREMENT," +
					"'known_site_id' INTEGER NOT NULL,"+
					"'hub_location' TEXT NOT NULL," +
					"'f_trial_id' INTEGER NOT NULL," + 
					"FOREIGN KEY(known_site_id) REFERENCES Site(site_id), " +
					"FOREIGN KEY(hub_location) REFERENCES Hub(location), " +
					"FOREIGN KEY(f_trial_id) REFERENCES Trial(trial_id)" +
					");";
		DoCommand(cmd_text, "--");
	}
	static void InsertTrial(int trial_id, string time, int latency, int bandwidth, string orgType) {
		string cmd_text = "INSERT INTO Trial(trial_id, time, latency, bandwidth, orgType, consensusType, hubDistribution, siteDistribution, siteNoise, numDropped) " +
			"VALUES (" + trial_id + ", '" + time + "', " + latency + ", " + bandwidth + ", '" +orgType+ "', '" + (HierarchyHub.consensusTeam.ToString()) + "', '" + MapInitializer.hubDistribution +"','"+MapInitializer.siteDistribution + "', '" + SiteScript.a_std.ToString()  + "','" + MapInitializer.dropNum.ToString() +  "');";
		DoCommand(cmd_text, "Insert trial #: " + trial_id);
	}

	static void InsertSite(int site_id, string location, float quality, int f_trial_id) {
		string cmd_text = "INSERT INTO Site(site_id, location, quality, f_trial_id) " +
			"VALUES (" + site_id + ", '" + location + "', " + ((int)(100.0 * quality)) + ", " + f_trial_id + ");";
		DoCommand(cmd_text, "Insert Site #: " + site_id);
	}

	static void InsertHub(string location, int chosen_site_id, int actual_site_id, int f_trial_id) {
		string cmd_text;

		string chosen_site_id_str;
		string actual_site_id_str;

		if (chosen_site_id != -1) {
			chosen_site_id_str = chosen_site_id.ToString();
		} else {
			chosen_site_id_str = "NULL";
		}

		if (actual_site_id != -1) {
			actual_site_id_str = actual_site_id.ToString();
		} else {
			actual_site_id_str = "NULL";
		}

		cmd_text = "INSERT INTO Hub(location, chosen_site_id, actual_site_id, f_trial_id) " +
			       "VALUES ('" + location + "', " + chosen_site_id_str + ", " + actual_site_id_str + "," + f_trial_id + ");";
		DoCommand(cmd_text, "Insert Hub: " + location + " - " + chosen_site_id +" - " + f_trial_id);
	}

	static void InsertSiteKnownByHub(GameObject s, HierarchyHub hh, int f_trial_id) {
		string cmd_text = "INSERT INTO SiteKnownByHub(known_site_id, hub_location, f_trial_id) VALUES (" + s.GetComponent<SiteScript>().site_id + ", '" + hh.GetPosition() + "', " + f_trial_id + ");";
		DoCommand(cmd_text, "---");

	}


	public static float GetCurrentGameTime() {
		return Time.time - startTime;
	}

	// Use this for initialization
	void Start() {
		Time.timeScale =7.0f;
		has_restarted = false;
		startTime = Time.time;
		Debug.unityLogger.logEnabled = true;

		//HierarchyHub.SetLatencyAndBandwdith(test.GetLatency(), test.GetBandwidth());
	}

	void RecordTrial() {
		//Debug.Log(dbPath);
		InsertTrial(MapInitializer.current_trial_id, Time.time.ToString(), HierarchyHub.latency, HierarchyHub.bandwidth, HierarchyHub.GetOrgType());
		RecordSites(MapInitializer.current_trial_id);
		RecordHubs(MapInitializer.current_trial_id);
		RecordSitesKnownByHubs(MapInitializer.current_trial_id);

		HierarchyHub.hubs.Clear();

	}

	static void RecordSites(int f_trial_id) {
		foreach (SiteScript s in SiteScript.sites) {
			InsertSite(s.GetSiteId(), s.toPos(), s.SampleQuality(true), f_trial_id);
		}
	}

	static void RecordHubs(int f_trial_id) {
		foreach (HierarchyHub hh in HierarchyHub.hubs) {
			InsertHub(hh.GetPosition(), hh.GetCommitSiteId(), hh.GetActualSwarmCommitId(), f_trial_id);
		}
	}



	static void RecordSitesKnownByHubs(int f_trial_id) {
		foreach (HierarchyHub hh in HierarchyHub.hubs) {
			foreach (GameObject s in hh.GetHubState().GetCopyOfKnownSites()) {
				InsertSiteKnownByHub(s, hh, f_trial_id);
			}
		}

	}

	static void RestartSim() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		SiteScript.sites.Clear();
	}

	float lastSecond = 0.0f;
	void FixedUpdate() {

		float time = GetCurrentGameTime();
		if (time - lastSecond > 50) {
			//Debug.Log(time.ToString() + " " +HierarchyHub.GetCurrentTime().ToString());
			lastSecond = time;
		}

		if (!has_restarted && GetCurrentGameTime()> HierarchyHub.MAX_TIME) {
			RecordTrial();
			foreach (HierarchyHub hh in HierarchyHub.hubs) {
				Debug.Log(hh.GetHubState().ToString());
			}
			Debug.Log("Test complete");
			has_restarted = true;
			
			
			//Debug.Log(dbPath);
			//RestartSim();

		}
		if (has_restarted && GetCurrentGameTime() > HierarchyHub.MAX_TIME + 5) {
			Time.timeScale = 0;RestartSim();
		}
	}
}
