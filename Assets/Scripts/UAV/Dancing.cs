﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dancing : DefaultStateBehavior {
		//private bool lockout = false;
	private static float dancing_time_limit = HierarchyHub.MAX_TIME * .1f;
	public static float GetDancingTimeLimit() {
		return dancing_time_limit;
	}

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		base.OnStateEnter(animator,stateInfo,layerIndex);
		script.waypoint = script.hubLocation;
		script.dancingTime *= script.estimate;
		//script.animator.SetFloat("DancingTime", script.dancingTime);
		script.numDancesLeft--;
		//Debug.Assert(script.site!=null);
        //FIXME Magic Color
        animator.GetComponent<Renderer>().material.color = Color.green;
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		//base.Refuel();
		if (!lockout && (HierarchyHub.GetCurrentTime() - state_start_time) > dancing_time_limit) {
			if (script.numDancesLeft == 0) {
				animator.SetTrigger("DanceIgnored");
			} else {
				animator.SetTrigger("Reassess");
			}
			lockout = true;
		}
		if (script.hasReachedWaypoint()) {
			script.waypoint = GenerateWanderWaypoint(script.hubLocation);
		}
		if (script.uniqueDancers.Count > 3) {
			script.animator.SetTrigger ("DancingThresholdReached");
		}

	}
}
