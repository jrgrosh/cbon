﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piping : DefaultStateBehavior {
	private static float piping_time_limit = HierarchyHub.MAX_TIME * .1f;
	//private bool lockout = false;

	public static float GetPipingTimeLimit() {
		return piping_time_limit;
	}

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		base.OnStateEnter(animator,stateInfo,layerIndex);
		script.waypoint = script.hubLocation;
		//Debug.Assert(script.site != null);
        //FIXME Magic Color
        animator.GetComponent<Renderer>().material.color = Color.gray;
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	//moving around will help with the collisions
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		if (script.hasReachedWaypoint()) {
			script.waypoint = base.GenerateWanderWaypoint(script.hubLocation);
		}
		if (script.uniquePipers.Count > 5) {
			script.animator.SetTrigger ("Quorum");
		}

		if (!lockout && (HierarchyHub.GetCurrentTime() - state_start_time) >piping_time_limit &&(HierarchyHub.GetCurrentTime() > .66f * HierarchyHub.MAX_TIME)) {
			animator.SetTrigger("PipeTimer");
			lockout = true;
		}

		//float PipingTimer = script.animator.GetFloat("PipingTimer");
		//script.animator.SetFloat("PipingTimer", PipingTimer - 1.0f);
	

	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
