﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UAVBehaviorScript : MonoBehaviour {


	public int numDancesLeft = 3;

	private static float HUM_INFLUENCE_COOLDOWN = HierarchyHub.MAX_TIME / 3;
	private float lastHumanInfluence = -1.0f * HUM_INFLUENCE_COOLDOWN;
	private bool hadHumanInfluence = false;

	// Use this for initialization
	public float speed = 5.0f;
	private Rigidbody2D rb2D;
	protected float alpha = 1.5f;
	public Vector2 waypoint = new Vector2(0, 0);
	public Vector2 hubLocation = new Vector2(0, 0);
	public SiteScript siteScript = null;

	public float siteQuality = -1.0f;


	public Animator animator = null;

	public GameObject site = null;

	public HashSet<GameObject> uniqueDancers = null;
	public HashSet<GameObject> uniquePipers = null;
	public HashSet<GameObject> rejected_sites = null;

	private List<string> statelist = null;



	public HierarchyHub hub = null;

	private bool registered = false;

	public void SetHierarchyHub(HierarchyHub h) {
		if (!registered) {
			hub = h;
			hub.SelfRegisterAgent(this.gameObject);
			registered = true;
		}
	}


	public float dancingTime = 2000;
	public float observingTime = 0.0f;

    //Checks State of UAV: Exploring, Assessing, Dancing, Observing, Resting, Piping, or Committing
	public string GetState(){
		for(int i=0; i<statelist.Count; i++){
			if(this.IsState(statelist[i])){
				return statelist[i];
			}
		}
		throw new InvalidOperationException("Current state not found among listed states");
	}

	//
	public bool IsState(string name){
		return this.GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).IsName (name);
	}

    //
	public void SetSite(GameObject s){
		this.site = s;
	}

	void Awake(){
		
	}

    private Renderer rend;
    public Color colorToTurnTo = Color.white;
    //When a UAV is made it must have all the possible states loaded.
    protected void Start () {
		statelist = new List<string>();
		statelist.Add( "Exploring");
		statelist.Add( "Assessing");
		statelist.Add( "Dancing");
		statelist.Add( "Observing");
		statelist.Add( "Resting");
		statelist.Add( "Piping");
		statelist.Add( "Committing");

		rb2D = gameObject.GetComponent<Rigidbody2D>();
		animator =gameObject.GetComponent<Animator> ();
		uniqueDancers = new HashSet<GameObject> ();
		uniquePipers = new HashSet<GameObject> ();
		rejected_sites = new HashSet<GameObject> ();
            //speed = 5.0f;
            //print (rb2D.position);

    
    rend = GetComponent<Renderer>();
    rend.material.color = colorToTurnTo;
}

	//Returns the 
	public Vector2 GetCurrentPosition(){
		return new Vector2 (transform.position.x, transform.position.y);
	}
	void MoveTowards(Vector2 waypoint){
		Vector2 cur_pos = GetCurrentPosition ();
		Vector2 direction_unnormed = waypoint - cur_pos;//new Vector2(rb2D.position.x,rb2D.position.y) - waypoint;
		Vector2 direction_normed = direction_unnormed.normalized;
		rb2D.AddForce(speed*direction_normed);
	}

    //
    //The next 3 functions are all very similar and somewhat
    //repetitive, except for the call of "MoveTowards" by "MoveTo"
    //
	public bool hasReached(Vector2 waypoint){
		Vector2 cur_pos = GetCurrentPosition ();
		if (Vector2.Distance (waypoint, cur_pos) < .3) {
			return true;
		} 
		return false;
	}

    //If the UAV has reached it's declared waypoint this returns true
	public bool hasReachedWaypoint(){
		return (Vector2.Distance(this.waypoint,GetCurrentPosition()) < .3);
	}

    //Somewhat redundant - returns true if declared waypoint is rea
	public bool MoveTo(Vector2 waypoint){
		if (hasReached (waypoint)) {
			return true;
		}
		MoveTowards (waypoint);
		return false;
		
	}

	//todo... have uavs use this when assessing
	public HashSet<GameObject> siteKeys = new HashSet<GameObject>();
	public float estimate = 0.0f;
	public void GiveKeys(HashSet<GameObject> newKeys) {
		siteKeys.UnionWith(newKeys);
	}


	public static bool IsState(GameObject gameobj, string name){
		return gameobj.GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).IsName (name);
	}
	//TODO: context - name for uavscript?
	//TODO: Document codew
	//TODO: Superclass for sitescript and uavbehavior script
	//TODO: how does movement work? Always moving towards a waypoint, waypoint changed when reached? or must specify waypoint? learning towards former option
	//TODO: create is state function, tags on swarm agents, change UAVs to swarm agents, switch statement here
	void OnTriggerEnter2D(Collider2D other){ //UAVs are trigger colliders... otherwise they bounce into each other 
		if (other.CompareTag("site") && this.GetState() == "Exploring" && !this.rejected_sites.Contains(other.gameObject)) {
			//this.site_report = sr;
			this.site = other.gameObject;
			animator.SetTrigger("SiteDiscovered");
			//Debug.Assert(this.site != null);
		}else if(other.CompareTag("swarm_agent") && other.GetComponent<UAVBehaviorScript>().hubLocation == this.hubLocation){
			string state = this.GetState();
			string other_state = other.GetComponent<UAVBehaviorScript>().GetState();
			GameObject o_site = other.GetComponent<UAVBehaviorScript>().site;
			if (o_site != null && rejected_sites.Contains(o_site)) {
				return;
			}
			
			switch(other_state){
				case "Dancing":
					//Debug.Assert(o_site!=null);
					if(state == "Observing"){
						float site_quality = other.gameObject.GetComponent<UAVBehaviorScript>().estimate;
						float pickiness = 1.0f;
						if(HierarchyHub.GetCurrentTime() < .25f * HierarchyHub.MAX_TIME){
							pickiness = 1.0f;
						}else if(HierarchyHub.GetCurrentTime() < .4f * HierarchyHub.MAX_TIME) {
							pickiness = .8f;
						}else if(HierarchyHub.GetCurrentTime() < .5f * HierarchyHub.MAX_TIME) {
							pickiness = .6f;
					}else if(HierarchyHub.GetCurrentTime() < .6f * HierarchyHub.MAX_TIME) {
							pickiness = .4f;
					} else if (HierarchyHub.GetCurrentTime() < .7f * HierarchyHub.MAX_TIME) {
							pickiness = .2f;
					}else{
							pickiness = .1f;
					}
						if(!this.rejected_sites.Contains(site) && Mathf.Pow(site_quality,2.0f) > pickiness ){	
								this.site= o_site;//STORE SITE
								//Debug.Log(site.transform.position.ToString() + " " + site_quality + " " + pickiness);
							
								animator.SetTrigger("SiteToInvestigate");
							} else{
								//this.rejected_sites.Add(o_site);
							}
					} else if(state == "Dancing" && this.site == o_site){
						this.uniqueDancers.Add(other.gameObject);
					}
					break;
				case "Piping":
					//Debug.Assert(o_site!=null);
					if(state == "Observing" || state == "Dancing" || state == "Resting"){	
						this.site=o_site;//STORE Site
						animator.SetTrigger("PipedByNeighbor");
					} else if(state == "Piping" && this.site == o_site){
						this.uniquePipers.Add(other.gameObject);
					}
					break;
			}
		}
	}


	// Update is called once per frame
	private float forbidLength = .05f * HierarchyHub.MAX_TIME;
	void FixedUpdate () {
		MoveTo(this.waypoint);
		/*if (this.forbidExpire.Count > 0 && HierarchyHub.GetCurrentTime() - forbidExpire.Peek() > forbidLength) {
			forbidExpire.Dequeue();
		}*/
        System.Console.WriteLine("Moved Some!");
    }

	/*public void InfluenceToCommitToSite(GameObject s) {
		if (Time.time - this.lastHumanInfluence < HUM_INFLUENCE_COOLDOWN) {
			return;
		}
		float r_val = UnityEngine.Random.value;
		if (r_val > .7) {
			this.SetSite(s);
			//Debug.Log("Successful influence - " + s.GetComponent<SiteScript>().toPos() + " " + r_val.ToString() + " "+ hubLocation.ToString());
		}
		//Debug.Log("Unsuccessful influence - " + s.GetComponent<SiteScript>().toPos() + " " + r_val.ToString() + " " + hubLocation.ToString());
		this.lastHumanInfluence = Time.time;
	}*/

	bool AcceptsHumanInfluence() {
		if (UnityEngine.Random.value < .3f && (HierarchyHub.GetCurrentTime() - this.lastHumanInfluence > HUM_INFLUENCE_COOLDOWN)) {
			this.lastHumanInfluence = HierarchyHub.GetCurrentTime();
			return true;
		}
		return false;
	}

	public void PromoteSite(GameObject s) {
		if (this.GetState() == "Observing" && this.AcceptsHumanInfluence()) {
			if (rejected_sites.Contains(s)){
				rejected_sites.Remove(s);
			}
			this.site = s;
			this.animator.SetTrigger("SiteToInvestigate"); 
		}
	}

	private Queue<float> forbidExpire = new Queue<float>();
	private float lastForbidTime = 0.0f;


	//private int numForbiddenCommitsLeft = 2;
	public void ForbidSite(GameObject s) {

		//if (this.site == s && (this.GetState() == "Piping" || this.GetState() == "Dancing" || this.GetState() == "Assessing") && numForbiddenCommitsLeft > 0){
		if (this.site == s && (this.GetState() == "Piping" || this.GetState() == "Dancing" || this.GetState() == "Assessing") ) {
			//this.site = null;
			if (HierarchyHub.GetCurrentTime() - lastForbidTime > 5.0f) {
				lastForbidTime = HierarchyHub.GetCurrentTime();
			} else {
				return;
			}

			this.animator.SetTrigger("Forbidden");
			//Debug.Log(this.GetInstanceID() + " has had site " + s.transform.position.ToString() + " forbidden");
			//forbidExpire.Enqueue(HierarchyHub.GetCurrentTime() + .1f * HierarchyHub.MAX_TIME);
			this.rejected_sites.Add(s);
			//numForbiddenCommitsLeft--;
		}
	}
	
	public void InhibitSite(GameObject s) {
		if ((this.GetState() == "Assessing" || this.GetState() == "Dancing") && this.AcceptsHumanInfluence()) {
			if (s == this.site) {
				this.animator.SetTrigger("Forbidden");
			}
		}
	}
	/*
	public void PromoteCommit(GameObject s) {
		if (this.GetState() == "Resting" && this.AcceptsHumanInfluence()) {
			this.site = s;
			this.animator.SetTrigger("PipedByNeighbor");
		}
	}

	*/
}
