﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultStateBehavior : StateMachineBehaviour {
	protected bool lockout = false;


	protected Animator animator = null;
	protected GameObject go = null;
	protected Rigidbody2D rb = null;
	protected UAVBehaviorScript script = null;
	protected float state_start_time = 0.0f;

    //TODO: this OnStateEnter and OnStateExit are called each frame. Fix so only called when state is entered;
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		lockout = false;
		this.animator = animator;
		go = animator.gameObject;
		rb = go.GetComponent<Rigidbody2D> ();
		script = go.GetComponent<UAVBehaviorScript> ();
		this.state_start_time = HierarchyHub.GetCurrentTime();
        
    }
		

	protected Vector2 GenerateWanderWaypoint(Vector2 center){
		float x = center.x + 3*(.5f-Random.value) ;
		float y = center.y + 3*(.5f-Random.value) ;
		return new Vector2 (x, y);
	}
}


public class Exploring : DefaultStateBehavior {
	private float alpha = 1.5f;
	private float explore_time_limit = HierarchyHub.MAX_TIME * .15f;
	//private bool lockout = false;

	override public void  OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		base.OnStateEnter(animator, stateInfo, layerIndex);
		script.waypoint = script.hubLocation;
        //FIXME Magic Color
        animator.GetComponent<Renderer>().material.color = Color.red;
    }
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex){
        if (!lockout && (HierarchyHub.GetCurrentTime() - this.state_start_time > explore_time_limit) ){
			animator.SetTrigger("ExploringDone");
			lockout = true;
		}

		//script.MoveTo (new Vector2 (7, 0));
		if (!script.hasReachedWaypoint()) {
			return;
		}	
		script.waypoint = GenerateLevyWaypoint ();
	}

	Vector2 GenerateLevyWaypoint(){
		Vector2 cur_pos = script.GetCurrentPosition ();
		float theta = Random.value * 4.0f * Mathf.PI;
		float f = Mathf.Pow (Random.value, -1.0f / alpha);
		return new Vector2 (cur_pos.x + f * Mathf.Cos (theta), cur_pos.y + f * Mathf.Sin (theta));
	}

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	//override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	//override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		//animator.gameObject.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (1, 1));
		//animator.gameObject.GetComponent<Explore>().speed *= .99f;
		//animator.gameObject.GetComponent<Explore>().fuel
	//}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
