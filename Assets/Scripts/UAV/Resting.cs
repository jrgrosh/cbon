﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resting : DefaultStateBehavior {
	private float resting_time_limit = HierarchyHub.MAX_TIME * .05f;
	//private bool lockout = false;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		base.OnStateEnter (animator, stateInfo, layerIndex);
		script.waypoint = script.hubLocation;
		float r = Random.value;
		//script.site = null;
		//Debug.Log("Random value: " + r);
		if (r < .3f) {
			

			//base.animator.SetTrigger ("DoneExploring");
		}
        //FIXME Magic Color
        animator.GetComponent<Renderer>().material.color = Color.cyan;
    }
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex){
	
		/*if (!script.MoveTo (script.hubLocation)) {
			state_start_time = HierarchyHub.GetCurrentTime();//want resting_time_limit to begin ticking away when at the hub
		}*/
		
		if (!lockout && (HierarchyHub.GetCurrentTime() - state_start_time > resting_time_limit)) {
		
			if (Random.value < 0.0f) {
				animator.SetTrigger("RestingDone");
			} else {
				animator.SetTrigger("BeginObserving");
			}
			lockout = true;
		}

	}
	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	//override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
