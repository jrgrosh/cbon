﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//maybe spends more time at the site, better the estimate.
//but as of now - 17 july 2018 - assessors wait at the site until they run out of fuel
public class Assessing : DefaultStateBehavior {
	//public bool lockout = false;
	private static float assessing_time_limit = HierarchyHub.MAX_TIME * .1f;

	public static float GetAssessingTimeLimit() {
		return assessing_time_limit;
	}

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		lockout = false;
		base.OnStateEnter(animator,stateInfo,layerIndex);
		//Debug.Assert(script.site != null);
		script.waypoint = script.site.GetComponent<SiteScript>().GetCurrentPosition();//script.siteScript.GetCurrentPosition();
        //FIXME Magic Color
        animator.GetComponent<Renderer>().material.color = Color.magenta;
    }
	 
	//TODO: site locations. easy locations for everything. inherited 
	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		//base.ConsumeFuel();
		if (script.hasReachedWaypoint()) {
			script.waypoint = GenerateWanderWaypoint(script.site.GetComponent<SiteScript>().GetCurrentPosition());
		}


		if (!lockout && (HierarchyHub.GetCurrentTime() - state_start_time > assessing_time_limit)) {
			animator.SetTrigger("AssessingDone");
			lockout = true ;
		}
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		if (script == null || script.hub ==  null ||script.site == null || script.site.GetComponent<SiteScript>() == null) {
			return;
		}
		
			script.estimate = script.site.GetComponent<SiteScript>().SampleQuality(false);
			//Debug.Log(this.script.hub.transform.position.ToString() + " received report that quality of site " + script.site.transform.position.ToString() + " is " + script.estimate.ToString());
				
			script.hub.ReportSite(script.site, script.estimate);
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
